/**
 *
 */
package top.ibase4j.core.base;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 *
 * @author ShenHuaJie
 *
 * @version 2016年6月3日 下午2:30:14
 *
 */
public interface BaseMapper<T extends BaseModel> extends com.baomidou.mybatisplus.core.mapper.BaseMapper<T> {

    List<Long> selectIdPage(@Param("cm") T params);

    List<Long> selectIdPage(@Param("cm") Map<String, Object> params);

    List<Long> selectIdPage(Page<Long> page, @Param("cm") Map<String, Object> params);

    List<Long> selectIdPage(Page<Long> page, @Param("cm") T params);

    List<T> selectPage(Page<Long> page, @Param("cm") Map<String, Object> params);

    Integer selectCount(@Param("cm") Map<String, Object> params);
}
